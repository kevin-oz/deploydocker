/*
*autor: Kevin Figueroa
*/

/*
  llenado de la tabla con los datos 
*/
function tabla(datos) {
    contenido.innerHTML=''
    for(let valor of datos){
        contenido.innerHTML += `
        <tr>
        <th scope="row">${valor.idTipoEstadoReserva}</th>
        <td>${valor.nombre}</td>
        <td>${valor.activo?"activo":"inactivo"}</td>
        <td>${valor.indicaAprobacion?"Si":"No"}</td>
        <td>${valor.observaciones}</td>
      </tr>      
    `
    }
}

/*
variables que almacenan los valores
del formulario
*/

var id,activo,aprob,nombre,obs;
id=document.getElementById("txtId");
nombre=document.getElementById("txtnombre");
activo=document.getElementById("activo_");
aprob=document.getElementById("apro_");
obs=document.getElementById("obs_");



/*
se invoca al dar submit,dentro se agg una funcion
donde se llama al metodo que hace post(create_)
*/
document.getElementById("frmregistro").addEventListener("submit",function(e){
    e.preventDefault();
    /*
    entity: objeto que contiene todos los valores que se han 
    introducido en el formulario
    */
    let entity = {
        activo: activo.checked,
        indicaAprobacion: aprob.checked,
        nombre: nombre.value,
        observaciones: obs.value 
    }

    //impresion en consola para verificar que se han guardado los datos en el objeto
console.log(entity)
/*
llamada al metodo que hace POST
*/
create_(url,entity)
findRange();
document.getElementById("frmregistro").reset(); 
})

/**
 * al dar click al boton btnModificar se desencadena la accion de tomar
 * los elementos del formulario y actualizarlo deacuerdo al id que se introduce
 */
document.getElementById("btnModificar").addEventListener("click",function(){
 
    /**
     * entity: objeto que contiene todos los valores que se han 
    introducido en el formulario
     */
    let entity = {
        activo: activo.checked,
        idTipoEstadoReserva: id.value,
        indicaAprobacion: aprob.checked,
        nombre: nombre.value,
        observaciones: obs.value 
    }

    //console.log(entity)
    edit_(url,entity)
    findRange();
    document.getElementById("frmregistro").reset(); 
})

/**
 * al dar click al boton brnEliminar se tomara el id del formulario
 * y se procedera a eliminar dicho registro de la base de datos
 */
document.getElementById("brnEliminar").addEventListener("click",function(){
    let idR= id.value;
    console.log(idR)
    eliminar_(url,idR)
    findRange();
    document.getElementById("frmregistro").reset(); 
});

//carga la cantidad de registros por defauld que tiene el findRange al iniciar el proyecto
window.onload = function (){
findRange();
};